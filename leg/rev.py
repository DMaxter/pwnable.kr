def main():
    # R11 is RBP
    # R11 - 0x10 is key

    # push LR <-- Return address
    # R11 = SP + 0x8
    # SP = SP - 0xc
    key = 0
    # R0 = *(0x8dc) <-- We know its in thumb mode because PC is 2 instructions ahead
    print(0x0006a4b0)
    # R1 = &key
    # R0 = *(0x8dc4)
    scanf(0x6a4bc)
    R2 = key1() + key2() + key3()
    R3 = key

def key1():
    return 0x8ce4

def key2():
    # push R11
    # R11 = SP
    # push R6
    R6 = 0x8d04 + 1
    # bx r6 <--- Keeps the thumb mode
    R3 = 0x8d08 + 4 # flags are updated
    # push r3
    # pop pc
    # pop r6
    return R3

def key3():
    return 0x8d80
